nginx-open-source
=========

###Build docker with opentracing

[Building the Ingress Controller Image](https://docs.nginx.com/nginx-ingress-controller/installation/building-ingress-controller-image/)

```shell
git clone git@github.com:andriimartynov/kubernetes-ingress.git //Release 1.12.0 with fixed opentracing lib
cd kubernetes-ingress
make debian-image-opentracing PREFIX=andriimartynov/kubernetes-ingress TARGET=container
```

###Install ingress

```shell
helm install demo-auth k8s/opentracing/helm/ingress
```


##Copy js files to ingress pod

```shell
kubectl cp k8s/nginx-open-source/js/main.js default/demo-auth-nginx-ingress-{pod_id}:/etc/nginx
kubectl cp k8s/nginx-open-source/js/jwt.js default/demo-auth-nginx-ingress-{pod_id}:/etc/nginx

````

###Delete default config map
```shell
kubectl delete ConfigMap demo-auth-nginx-ingress
````


###Install application chart

```shell
helm install nginx-open-source-demo-auth k8s/opentracing/helm/auth
````

###Test


```shell
curl -X GET 'http://master/tests' -H 'Accept: application/json' -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhbWFydHlub3ZAbmV3YWdlc29sLmNvIiwidXNlcl91dWlkIjoiT1BFUkFUT1ItODJkZmE0ZWEtZDU3YS00ZmI3LTg5M2EtNWNhZTU3NWFmMTQ2Iiwicm9sZSI6IlJPTEU0IiwibG9naW5fdHlwZSI6ImVtYWlsIiwiYnJhbmRJZCI6ImNhc2lub185OTlfZGsiLCJpc3MiOiJIUlpOIEF1dGgiLCJleHAiOjE2MjY3OTEwMDUsImRlcGFydG1lbnQiOiJBRE1JTklTVFJBVElPTiIsInNlc3Npb25fdXVpZCI6IjQ2Mzc3ODk1LTI1MTktNGI4Ni04NmI3LTkxMmVjNDMzNDQ1MiIsInNlc3Npb25fc3RhcnQiOjE2MjY3ODk2ODUsImlhdCI6MTYyNjc4OTY4NX0.bYuaUffrz2qTazpeyJSCo05TxA8RjFwsxGedtVFLtZ530ITXHLZPD8jNDB-d4_tux75H1Mj80_BFJxGGyKt55g"
```

```shell
kubectl logs auth
...
GET HashMap( -> , method -> GET, uri -> /tests, token -> eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhbWFydHlub3ZAbmV3YWdlc29sLmNvIiwidXNlcl91dWlkIjoiT1BFUkFUT1ItODJkZmE0ZWEtZDU3YS00ZmI3LTg5M2EtNWNhZTU3NWFmMTQ2Iiwicm9sZSI6IlJPTEU0IiwibG9naW5fdHlwZSI6ImVtYWlsIiwiYnJhbmRJZCI6ImNhc2lub185OTlfZGsiLCJpc3MiOiJIUlpOIEF1dGgiLCJleHAiOjE2MjY3OTEwMDUsImRlcGFydG1lbnQiOiJBRE1JTklTVFJBVElPTiIsInNlc3Npb25fdXVpZCI6IjQ2Mzc3ODk1LTI1MTktNGI4Ni04NmI3LTkxMmVjNDMzNDQ1MiIsInNlc3Npb25fc3RhcnQiOjE2MjY3ODk2ODUsImlhdCI6MTYyNjc4OTY4NX0.bYuaUffrz2qTazpeyJSCo05TxA8RjFwsxGedtVFLtZ530ITXHLZPD8jNDB-d4_tux75H1Mj80_BFJxGGyKt55g, credential -> operator)
headers List(Content-Type: none/none, Content-Length: 0, Timeout-Access: <function1>, X-Original-URI: /tests, Host: default-master-ingress-master-auth-9090, Connection: close, User-Agent: curl/7.64.1, Accept: application/json, Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhbWFydHlub3ZAbmV3YWdlc29sLmNvIiwidXNlcl91dWlkIjoiT1BFUkFUT1ItODJkZmE0ZWEtZDU3YS00ZmI3LTg5M2EtNWNhZTU3NWFmMTQ2Iiwicm9sZSI6IlJPTEU0IiwibG9naW5fdHlwZSI6ImVtYWlsIiwiYnJhbmRJZCI6ImNhc2lub185OTlfZGsiLCJpc3MiOiJIUlpOIEF1dGgiLCJleHAiOjE2MjY3OTEwMDUsImRlcGFydG1lbnQiOiJBRE1JTklTVFJBVElPTiIsInNlc3Npb25fdXVpZCI6IjQ2Mzc3ODk1LTI1MTktNGI4Ni04NmI3LTkxMmVjNDMzNDQ1MiIsInNlc3Npb25fc3RhcnQiOjE2MjY3ODk2ODUsImlhdCI6MTYyNjc4OTY4NX0.bYuaUffrz2qTazpeyJSCo05TxA8RjFwsxGedtVFLtZ530ITXHLZPD8jNDB-d4_tux75H1Mj80_BFJxGGyKt55g)
GET Map()
headers List(Content-Type: none/none, Content-Length: 0, Timeout-Access: <function1>, hzdk-trace-id: 721bdf8bc0895891:0a275f972882b4c0:721bdf8bc0895891:1, Host: master, X-Real-Ip: 192.168.65.3, X-Forwarded-For: 192.168.65.3, X-Forwarded-Host: master, X-Forwarded-Port: 80, X-Forwarded-Proto: http, Connection: close, User-Agent: curl/7.64.1, Accept: application/json, Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhbWFydHlub3ZAbmV3YWdlc29sLmNvIiwidXNlcl91dWlkIjoiT1BFUkFUT1ItODJkZmE0ZWEtZDU3YS00ZmI3LTg5M2EtNWNhZTU3NWFmMTQ2Iiwicm9sZSI6IlJPTEU0IiwibG9naW5fdHlwZSI6ImVtYWlsIiwiYnJhbmRJZCI6ImNhc2lub185OTlfZGsiLCJpc3MiOiJIUlpOIEF1dGgiLCJleHAiOjE2MjY3OTEwMDUsImRlcGFydG1lbnQiOiJBRE1JTklTVFJBVElPTiIsInNlc3Npb25fdXVpZCI6IjQ2Mzc3ODk1LTI1MTktNGI4Ni04NmI3LTkxMmVjNDMzNDQ1MiIsInNlc3Npb25fc3RhcnQiOjE2MjY3ODk2ODUsImlhdCI6MTYyNjc4OTY4NX0.bYuaUffrz2qTazpeyJSCo05TxA8RjFwsxGedtVFLtZ530ITXHLZPD8jNDB-d4_tux75H1Mj80_BFJxGGyKt55g)
```

No trace id in auth subrequest [nginx-opentracing lacks subrequest support](https://github.com/opentracing-contrib/nginx-opentracing/issues/174#issuecomment-878160264)

###UnInstall application chart

```shell
helm uninstall nginx-open-source-demo-auth k8s/nginx-open-source/helm
```

##Documentation
[Opentracing](https://opentracing.io)

[NGINX Ingress Controller](https://docs.nginx.com/nginx-ingress-controller/intro/overview/)

[Building the Ingress Controller Image](https://docs.nginx.com/nginx-ingress-controller/installation/building-ingress-controller-image/)

[Third Party Modules](https://docs.nginx.com/nginx-ingress-controller/third-party-modules/opentracing/)

[Custom Annotations](https://docs.nginx.com/nginx-ingress-controller/configuration/ingress-resources/custom-annotations/)

[Custom Templates](https://docs.nginx.com/nginx-ingress-controller/configuration/global-configuration/custom-templates/)

[Getting Arbitrary Field from JWT as nginx Variable](http://nginx.org/en/docs/njs/examples.html#jwt_field)

[Nginx opentracing](https://github.com/opentracing-contrib/nginx-opentracing)
