local http = require "resty.http"
local json = require "cjson"

local decode_base64 = ngx.decode_base64
local find = string.find
local pcall = pcall
local rep = string.rep
local sub = string.sub
local unpack = unpack

local CustomAuth = {}

CustomAuth.PRIORITY = 900

CustomAuth.JWT_TOKEN_MISSING = { error = "error.jwt_token.missing" }

CustomAuth.CONTENT_APPLICATION_JSON = { ["Content-Type"] = "application/json; charset=utf-8" }

function CustomAuth:access()

  --- base 64 decode
  -- @param input String to base64 decode
  -- @return Base64 decoded string
  local function base64_decode(input)
    local remainder = #input % 4

    if remainder > 0 then
      local padlen = 4 - remainder
      input = input .. rep("=", padlen)
    end

    input = input:gsub("-", "+"):gsub("_", "/")
    return decode_base64(input)
  end

  --- Tokenize a string by delimiter
  -- Used to separate the header, claims and signature part of a JWT
  -- @param str String to tokenize
  -- @param div Delimiter
  -- @param len Number of parts to retrieve
  -- @return A table of strings
  local function tokenize(str, div, len)
    local result, pos = {}, 0

    local iter = function()
      return find(str, div, pos, true)
    end

    for st, sp in iter do
      result[#result + 1] = sub(str, pos, st-1)
      pos = sp + 1
      len = len - 1
      if len <= 1 then
        break
      end
    end

    result[#result + 1] = sub(str, pos)
    return result
  end

  --- Parse a JWT
  -- Parse a JWT and validate header values.
  -- @param token JWT to parse
  -- @return A table containing base64 and decoded headers, claims and signature
  local function decode_token(token)
    -- Get b64 parts
    local header_64, claims_64, signature_64 = unpack(tokenize(token, ".", 3))

    kong.log("claims_64=", base64_decode(claims_64))

    -- Decode JSON
    local ok, claims = pcall(function()
      return json.decode(base64_decode(claims_64))
    end)
    if not ok then
      return nil, "invalid JSON"
    end

    if not claims then
      return nil, "invalid claims"
    end

    return {
      token = token,
      header_64 = header_64,
      claims_64 = claims_64,
      signature_64 = signature_64,
      claims = claims
    }
  end

  local service_url = "/" .. ngx.var.request_uri

  if service_url:find("^/public") ~= nil then
    kong.log("x-fwd-for=", kong.request.get_header("X-Forwarded-For"), " pass-through public")
    return
  end

  local token = ngx.var.http_Authorization

  local parsedToken, err = decode_token(token)
  if err then
    kong.log("jwt parsing error=", err, "token=", token)
    return kong.response.exit(ngx.HTTP_UNAUTHORIZED, CustomAuth.JWT_TOKEN_MISSING)
  end

  local role = ""
  if parsedToken.claims.role == "PLAYER" then role = "PLAYER"
    else role = "OPERATOR"
  end

  if ngx.var.http_Authorization then
    token = token:gsub("(Bearer)%s+", "")
  else
    return kong.response.exit(ngx.HTTP_UNAUTHORIZED, CustomAuth.JWT_TOKEN_MISSING)
  end

  local httpc = http.new()

  local res, err = httpc:request_uri("http://auth:9090/auth", {
    query = {
      token = token,
      method = ngx.var.request_method,
      uri = ngx.var.request_uri,
      role = role
    },
    method = "GET",
    headers = CustomAuth.CONTENT_APPLICATION_JSON,
  })

  if not res then
    kong.log.err("/access/validate error ", err)
    return kong.response.exit(ngx.HTTP_INTERNAL_SERVER_ERROR, err)
  end

  kong.log("x-fwd-for=", kong.request.get_header("X-Forwarded-For"), ", status=", res.status, ", body=", res.body)

  if res.status == ngx.HTTP_OK then
    return
  end

  return kong.response.exit(res.status, res.body, CustomAuth.CONTENT_APPLICATION_JSON)
end

return CustomAuth
