nginx-open-source
=========

###Installation

[Installation with Helm](https://docs.konghq.com/kubernetes-ingress-controller/1.3.x/deployment/k4k8s/)


##Create configmap with custom auth plugin
[Setting up custom plugin in Kubernetes environment](https://docs.konghq.com/kubernetes-ingress-controller/1.3.x/guides/setting-up-custom-plugins/)

```shell
kubectl create configmap kong-plugin-custom-auth --from-file=k8s/kong/plugins/custom-auth -n default

````

```shell
helm repo add kong https://charts.konghq.com
helm repo update

helm install kong/kong --generate-name --set ingressController.installCRDs=false --values k8s/kong/helm/kong/values.yaml
```

###Install application chart

```shell
helm install kong-demo-auth k8s/kong/helm/auth
````

###Test


```shell
curl -X GET 'http://master/health'  -H 'Accept: application/json' -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhbWFydHlub3ZAbmV3YWdlc29sLmNvIiwidXNlcl91dWlkIjoiT1BFUkFUT1ItODJkZmE0ZWEtZDU3YS00ZmI3LTg5M2EtNWNhZTU3NWFmMTQ2Iiwicm9sZSI6IlJPTEU0IiwibG9naW5fdHlwZSI6ImVtYWlsIiwiYnJhbmRJZCI6ImNhc2lub185OTlfZGsiLCJpc3MiOiJIUlpOIEF1dGgiLCJleHAiOjE2MjY3OTEwMDUsImRlcGFydG1lbnQiOiJBRE1JTklTVFJBVElPTiIsInNlc3Npb25fdXVpZCI6IjQ2Mzc3ODk1LTI1MTktNGI4Ni04NmI3LTkxMmVjNDMzNDQ1MiIsInNlc3Npb25fc3RhcnQiOjE2MjY3ODk2ODUsImlhdCI6MTYyNjc4OTY4NX0.bYuaUffrz2qTazpeyJSCo05TxA8RjFwsxGedtVFLtZ530ITXHLZPD8jNDB-d4_tux75H1Mj80_BFJxGGyKt55g"
```

```shell
kctl logs auth
...
GET Map(method -> GET, role -> OPERATOR, uri -> /health, token -> eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhbWFydHlub3ZAbmV3YWdlc29sLmNvIiwidXNlcl91dWlkIjoiT1BFUkFUT1ItODJkZmE0ZWEtZDU3YS00ZmI3LTg5M2EtNWNhZTU3NWFmMTQ2Iiwicm9sZSI6IlJPTEU0IiwibG9naW5fdHlwZSI6ImVtYWlsIiwiYnJhbmRJZCI6ImNhc2lub185OTlfZGsiLCJpc3MiOiJIUlpOIEF1dGgiLCJleHAiOjE2MjY3OTEwMDUsImRlcGFydG1lbnQiOiJBRE1JTklTVFJBVElPTiIsInNlc3Npb25fdXVpZCI6IjQ2Mzc3ODk1LTI1MTktNGI4Ni04NmI3LTkxMmVjNDMzNDQ1MiIsInNlc3Npb25fc3RhcnQiOjE2MjY3ODk2ODUsImlhdCI6MTYyNjc4OTY4NX0.bYuaUffrz2qTazpeyJSCo05TxA8RjFwsxGedtVFLtZ530ITXHLZPD8jNDB-d4_tux75H1Mj80_BFJxGGyKt55g)
headers List(Content-Type: application/json, Content-Length: 0, Timeout-Access: <function1>, User-Agent: lua-resty-http/0.14 (Lua) ngx_lua/10019, Host: auth:9090)
```

###UnInstall application chart

```shell
helm uninstall kong-demo-auth k8s/kong/helm/auth
```

##Documentation
[Kubernetes Ingress Controller](https://docs.konghq.com/kubernetes-ingress-controller/1.3.x/)

[Custom Authentication and Authorization Framework with Kong](https://konghq.com/blog/custom-authentication-and-authorization-framework-with-kong/)

[kong plugins jwt_parser](https://github.com/Kong/kong/blob/master/kong/plugins/jwt/jwt_parser.lua)
