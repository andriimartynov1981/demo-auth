Ambassador
=========

###Installation

[Ambassador Edge Stack quick start](https://www.getambassador.io/docs/edge-stack/latest/tutorials/getting-started/)

```shell
edgectl install
```


###Install application chart

```shell
kubectl create ns master && kubectl create ns slave && helm install ambassadore-demo-auth k8s/ambassador/helm
````

###Test


```shell
curl -Lk http://localhost/master/health
```

```shell
kctl -n master logs auth
2021-08-06 15:09:34,253 INFO  akka.event.slf4j.Slf4jLogger - Slf4jLogger started
2021-08-06 15:09:36,376 INFO  akka.actor.typed.ActorSystem - HTTP server started for auth port 9090
2021-08-06 15:09:36,757 INFO  akka.actor.typed.ActorSystem - GRPC server started for auth port 9091
2021-08-06 15:09:36,801 INFO  akka.actor.typed.ActorSystem - Application started successfully
2021-08-06 15:10:50,330 INFO  c.n.d.service.AuthorizationService - request: CheckRequest(Some(AttributeContext(Some(Peer(Some(Address(SocketAddress(SocketAddress(TCP,192.168.65.3,PortValue(63304),,false,UnknownFieldSet(Map()))),UnknownFieldSet(Map()))),,Map(),,,UnknownFieldSet(Map()))),Some(Peer(Some(Address(SocketAddress(SocketAddress(TCP,10.1.0.13,PortValue(8443),,false,UnknownFieldSet(Map()))),UnknownFieldSet(Map()))),,Map(),,,UnknownFieldSet(Map()))),Some(Request(Some(Timestamp(1628262649,918826600,UnknownFieldSet(Map()))),Some(HttpRequest(745457157097262015,GET,HashMap(accept -> */*, x-request-id -> e8c3ba91-c63d-4205-87d2-4b37949a9466, :authority -> localhost, user-agent -> curl/7.64.1, :method -> GET, :path -> /master/health, x-forwarded-for -> 192.168.65.3, x-envoy-internal -> true, x-forwarded-proto -> https),/master/health,localhost,,,,0,HTTP/1.1,,UnknownFieldSet(Map()))),UnknownFieldSet(Map()))),Map(),Some(Metadata(Map(),UnknownFieldSet(Map()))),UnknownFieldSet(Map()))),UnknownFieldSet(Map()))
2021-08-06 15:10:50,442 INFO  c.n.d.service.AuthorizationService - response: CheckResponse(Some(Status(400,,List(),UnknownFieldSet(Map()))),DeniedResponse(DeniedHttpResponse(Some(HttpStatus(Forbidden,UnknownFieldSet(Map()))),List(HeaderValueOption(Some(HeaderValue(Content-Type,application/json,UnknownFieldSet(Map()))),None,UnknownFieldSet(Map()))),{
  "isValid" : false,
  "jwtError" : "ACCESS_FORBIDDEN"
},UnknownFieldSet(Map()))),UnknownFieldSet(Map()))
```

second try

```shell
curl -Lk http://localhost/master/health
```

```shell
kctl -n master logs auth
2021-08-06 15:09:34,253 INFO  akka.event.slf4j.Slf4jLogger - Slf4jLogger started
2021-08-06 15:09:36,376 INFO  akka.actor.typed.ActorSystem - HTTP server started for auth port 9090
2021-08-06 15:09:36,757 INFO  akka.actor.typed.ActorSystem - GRPC server started for auth port 9091
2021-08-06 15:09:36,801 INFO  akka.actor.typed.ActorSystem - Application started successfully
2021-08-06 15:10:50,330 INFO  c.n.d.service.AuthorizationService - request: CheckRequest(Some(AttributeContext(Some(Peer(Some(Address(SocketAddress(SocketAddress(TCP,192.168.65.3,PortValue(63304),,false,UnknownFieldSet(Map()))),UnknownFieldSet(Map()))),,Map(),,,UnknownFieldSet(Map()))),Some(Peer(Some(Address(SocketAddress(SocketAddress(TCP,10.1.0.13,PortValue(8443),,false,UnknownFieldSet(Map()))),UnknownFieldSet(Map()))),,Map(),,,UnknownFieldSet(Map()))),Some(Request(Some(Timestamp(1628262649,918826600,UnknownFieldSet(Map()))),Some(HttpRequest(745457157097262015,GET,HashMap(accept -> */*, x-request-id -> e8c3ba91-c63d-4205-87d2-4b37949a9466, :authority -> localhost, user-agent -> curl/7.64.1, :method -> GET, :path -> /master/health, x-forwarded-for -> 192.168.65.3, x-envoy-internal -> true, x-forwarded-proto -> https),/master/health,localhost,,,,0,HTTP/1.1,,UnknownFieldSet(Map()))),UnknownFieldSet(Map()))),Map(),Some(Metadata(Map(),UnknownFieldSet(Map()))),UnknownFieldSet(Map()))),UnknownFieldSet(Map()))
2021-08-06 15:10:50,442 INFO  c.n.d.service.AuthorizationService - response: CheckResponse(Some(Status(400,,List(),UnknownFieldSet(Map()))),DeniedResponse(DeniedHttpResponse(Some(HttpStatus(Forbidden,UnknownFieldSet(Map()))),List(HeaderValueOption(Some(HeaderValue(Content-Type,application/json,UnknownFieldSet(Map()))),None,UnknownFieldSet(Map()))),{
  "isValid" : false,
  "jwtError" : "ACCESS_FORBIDDEN"
},UnknownFieldSet(Map()))),UnknownFieldSet(Map()))
2021-08-06 15:10:53,896 INFO  c.n.d.service.AuthorizationService - request: CheckRequest(Some(AttributeContext(Some(Peer(Some(Address(SocketAddress(SocketAddress(TCP,192.168.65.3,PortValue(63356),,false,UnknownFieldSet(Map()))),UnknownFieldSet(Map()))),,Map(),,,UnknownFieldSet(Map()))),Some(Peer(Some(Address(SocketAddress(SocketAddress(TCP,10.1.0.13,PortValue(8443),,false,UnknownFieldSet(Map()))),UnknownFieldSet(Map()))),,Map(),,,UnknownFieldSet(Map()))),Some(Request(Some(Timestamp(1628262653,889324200,UnknownFieldSet(Map()))),Some(HttpRequest(7087116012565186167,GET,HashMap(accept -> */*, x-request-id -> 4c00d2e9-9a6f-47ba-91e2-eaecb9e1865b, :authority -> localhost, user-agent -> curl/7.64.1, :method -> GET, :path -> /master/health, x-forwarded-for -> 192.168.65.3, x-envoy-internal -> true, x-forwarded-proto -> https),/master/health,localhost,,,,0,HTTP/1.1,,UnknownFieldSet(Map()))),UnknownFieldSet(Map()))),Map(),Some(Metadata(Map(),UnknownFieldSet(Map()))),UnknownFieldSet(Map()))),UnknownFieldSet(Map()))
2021-08-06 15:10:53,896 INFO  c.n.d.service.AuthorizationService - response: CheckResponse(None,OkResponse(OkHttpResponse(List(),UnknownFieldSet(Map()))),UnknownFieldSet(Map()))
2021-08-06 15:10:54,055 INFO  c.n.c.akka_http.HealthController - {
  "status" : "UP",
  "states" : [
  ]
}
```

slave

```shell
curl -Lk http://localhost/slave/health
```

```shell
kctl -n slave logs auth
2021-08-06 15:09:34,329 INFO  akka.event.slf4j.Slf4jLogger - Slf4jLogger started
2021-08-06 15:09:36,368 INFO  akka.actor.typed.ActorSystem - HTTP server started for auth port 9090
2021-08-06 15:09:36,763 INFO  akka.actor.typed.ActorSystem - GRPC server started for auth port 9091
2021-08-06 15:09:36,798 INFO  akka.actor.typed.ActorSystem - Application started successfully
2021-08-06 15:10:08,101 INFO  c.n.d.service.AuthorizationService - request: CheckRequest(Some(AttributeContext(Some(Peer(Some(Address(SocketAddress(SocketAddress(TCP,192.168.65.3,PortValue(62844),,false,UnknownFieldSet(Map()))),UnknownFieldSet(Map()))),,Map(),,,UnknownFieldSet(Map()))),Some(Peer(Some(Address(SocketAddress(SocketAddress(TCP,10.1.0.13,PortValue(8443),,false,UnknownFieldSet(Map()))),UnknownFieldSet(Map()))),,Map(),,,UnknownFieldSet(Map()))),Some(Request(Some(Timestamp(1628262607,625204000,UnknownFieldSet(Map()))),Some(HttpRequest(15100487555851607685,GET,HashMap(accept -> */*, x-request-id -> cc054757-c5fd-4c0f-a8c4-b24e04858e68, :authority -> localhost, user-agent -> curl/7.64.1, :method -> GET, :path -> /slave/health, x-forwarded-for -> 192.168.65.3, x-envoy-internal -> true, x-forwarded-proto -> https),/slave/health,localhost,,,,0,HTTP/1.1,,UnknownFieldSet(Map()))),UnknownFieldSet(Map()))),Map(),Some(Metadata(Map(),UnknownFieldSet(Map()))),UnknownFieldSet(Map()))),UnknownFieldSet(Map()))
2021-08-06 15:10:08,211 INFO  c.n.d.service.AuthorizationService - response: CheckResponse(Some(Status(400,,List(),UnknownFieldSet(Map()))),DeniedResponse(DeniedHttpResponse(Some(HttpStatus(Forbidden,UnknownFieldSet(Map()))),List(HeaderValueOption(Some(HeaderValue(Content-Type,application/json,UnknownFieldSet(Map()))),None,UnknownFieldSet(Map()))),{
  "isValid" : false,
  "jwtError" : "ACCESS_FORBIDDEN"
},UnknownFieldSet(Map()))),UnknownFieldSet(Map()))
```

###UnInstall application chart

```shell
helm uninstall ambassadore-demo-auth && kubectl delete ns master && kubectl delete ns slave
```
