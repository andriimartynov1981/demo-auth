Istio Ingress Gateway Ext Auth
===

The guide illustrates how to set up external authorization for Istio Ingress Gateway.

In this guide the [httpbin](https://httpbin.org) is used as a backend microservice and the
[Ext Authz Service](https://github.com/istio/istio/tree/master/samples/extauthz) is used as
an external authorization provider.

1. Install Istio


     istioctl install --set profile=default -y


2. Enable Istio sidecar injection


    kubectl label namespace {target_ns} istio-injection=enabled


4. Configure gateway resource


    kubectl apply -f gw.yaml -n {target_ns}

5. Install `httpbin` service


    kubectl apply -f httpbin.yaml -n {target_ns}

6. Configure the `httpbin` virtual service


    kubectl apply -f httpbin-vs.yaml -n {target_ns}

7. Install `ext-authz` service


    kubectl apply -f ext-authz.yaml -n {target_ns}

8. Configure Istio extension provider


    kubectl edit -n istio-system cm istio

Add the following snippet to the `configmap`:


    extensionProviders:
    - name: "sample-ext-authz-grpc"
      envoyExtAuthzGrpc:
        service: "ext-autzh.{target_ns}.svc.cluster.local"
        port: "9000"
        includeRequestHeadersInCheck: ["x-ext-authz"]

10. Apply custom authorization policy


    kubectl apply -f custom-authorization-policy.yaml -n istio-system

11. Verify that external authorization applied to all calls routing to the `httpbin` service.
In order to pass the authorization the `x-ext-authz: allow` header should be supplied.


    curl -s -H "Host:api.test.hrzn.io" http://{k8s_external_ip}/httpbin/uuid
    denied by ext_authz for not found header `x-ext-authz: allow` in the request%

    curl -s -H "Host:api.test.hrzn.io" -H "x-ext-authz: allow" http://{k8s_external_ip}/httpbin/uuid
    {
      "uuid": "4493743b-0347-427a-ad27-c63bf26b0bf5"
    }
