function jwt(data) {
  var parts = data.split('.').slice(0, 2)
    .map(v => Buffer.from(v, 'base64url').toString())
    .map(JSON.parse);
  return {headers: parts[0], payload: parts[1]};
}

export default function jwt_payload(r) {
  var authorization = r.headersIn.Authorization;
  if (authorization !== null && authorization !== '') {
    return jwt(authorization.slice(7)).payload.role.toLowerCase();
  }
  return "";
}
