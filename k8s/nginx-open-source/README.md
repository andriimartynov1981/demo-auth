nginx-open-source
=========

###Installation

[Installation with Helm](https://docs.nginx.com/nginx-ingress-controller/installation/installation-with-helm/)

```shell
helm repo add nginx-stable https://helm.nginx.com/stable
helm repo update
helm install demo-auth nginx-stable/nginx-ingress
```

##Copy js files to ingress pod

```shell
kubectl cp k8s/nginx-open-source/js/main.js default/demo-auth-nginx-ingress-{pod_id}:/etc/nginx
kubectl cp k8s/nginx-open-source/js/jwt.js default/demo-auth-nginx-ingress-{pod_id}:/etc/nginx

````

###Delete default config map
```shell
kubectl delete ConfigMap demo-auth-nginx-ingress
````


###Install application chart

```shell
helm install nginx-open-source-demo-auth k8s/nginx-open-source/helm
````

###Test


```shell
curl -X GET 'http://master/health'  -H 'Accept: application/json' -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhbWFydHlub3ZAbmV3YWdlc29sLmNvIiwidXNlcl91dWlkIjoiT1BFUkFUT1ItODJkZmE0ZWEtZDU3YS00ZmI3LTg5M2EtNWNhZTU3NWFmMTQ2Iiwicm9sZSI6IlJPTEU0IiwibG9naW5fdHlwZSI6ImVtYWlsIiwiYnJhbmRJZCI6ImNhc2lub185OTlfZGsiLCJpc3MiOiJIUlpOIEF1dGgiLCJleHAiOjE2MjY3OTEwMDUsImRlcGFydG1lbnQiOiJBRE1JTklTVFJBVElPTiIsInNlc3Npb25fdXVpZCI6IjQ2Mzc3ODk1LTI1MTktNGI4Ni04NmI3LTkxMmVjNDMzNDQ1MiIsInNlc3Npb25fc3RhcnQiOjE2MjY3ODk2ODUsImlhdCI6MTYyNjc4OTY4NX0.bYuaUffrz2qTazpeyJSCo05TxA8RjFwsxGedtVFLtZ530ITXHLZPD8jNDB-d4_tux75H1Mj80_BFJxGGyKt55g"
```

```shell
kctl logs auth
...
GET HashMap( -> , method -> GET, uri -> /health, token -> eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhbWFydHlub3ZAbmV3YWdlc29sLmNvIiwidXNlcl91dWlkIjoiT1BFUkFUT1ItODJkZmE0ZWEtZDU3YS00ZmI3LTg5M2EtNWNhZTU3NWFmMTQ2Iiwicm9sZSI6IlJPTEU0IiwibG9naW5fdHlwZSI6ImVtYWlsIiwiYnJhbmRJZCI6ImNhc2lub185OTlfZGsiLCJpc3MiOiJIUlpOIEF1dGgiLCJleHAiOjE2MjY3OTEwMDUsImRlcGFydG1lbnQiOiJBRE1JTklTVFJBVElPTiIsInNlc3Npb25fdXVpZCI6IjQ2Mzc3ODk1LTI1MTktNGI4Ni04NmI3LTkxMmVjNDMzNDQ1MiIsInNlc3Npb25fc3RhcnQiOjE2MjY3ODk2ODUsImlhdCI6MTYyNjc4OTY4NX0.bYuaUffrz2qTazpeyJSCo05TxA8RjFwsxGedtVFLtZ530ITXHLZPD8jNDB-d4_tux75H1Mj80_BFJxGGyKt55g, credential -> operator)
```

###UnInstall application chart

```shell
helm uninstall nginx-open-source-demo-auth k8s/nginx-open-source/helm
```

##Documentation
[NGINX Ingress Controller](https://docs.nginx.com/nginx-ingress-controller/intro/overview/)

[Building the Ingress Controller Image](https://docs.nginx.com/nginx-ingress-controller/installation/building-ingress-controller-image/)

[Third Party Modules](https://docs.nginx.com/nginx-ingress-controller/third-party-modules/opentracing/)

[Custom Annotations](https://docs.nginx.com/nginx-ingress-controller/configuration/ingress-resources/custom-annotations/)

[Custom Templates](https://docs.nginx.com/nginx-ingress-controller/configuration/global-configuration/custom-templates/)

[Getting Arbitrary Field from JWT as nginx Variable](http://nginx.org/en/docs/njs/examples.html#jwt_field)
