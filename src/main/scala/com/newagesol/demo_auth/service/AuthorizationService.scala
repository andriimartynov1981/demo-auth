package com.newagesol.demo_auth.service

import com.google.rpc.Status
import com.newagesol.common.akka_bootstrap.{ConfigModule, SystemModule}
import com.newagesol.demo_auth.service.AuthorizationService.{respForbidden, respOk}
import io.circe.Printer
import io.envoyproxy.envoy.`type`.{HttpStatus, StatusCode}
import io.envoyproxy.envoy.api.v2.core.{HeaderValue, HeaderValueOption}
import io.envoyproxy.envoy.service.auth.v2.{Authorization, CheckRequest, CheckResponse, DeniedHttpResponse, OkHttpResponse}

import scala.concurrent.Future
import scala.util.Random

trait AuthorizationService { this: ConfigModule  with SystemModule =>

  private val log = loggingAdapter[AuthorizationService]


  lazy val authorizationService: Authorization = new Authorization {
    def check(in: CheckRequest): Future[CheckResponse] = {
      log.info(s"request: $in")

      val res = if (Random.nextInt(1000) % 2 == 0) respOk
      else respForbidden


      log.info(s"response: $res")
      Future.successful(res)
    }
  }
}

object AuthorizationService {
  case class AccessValidationResponse(isValid: Boolean, jwtError: Option[String]) {
    private[AccessValidationResponse] def this(error: String) = this(false, Some(error))

    import io.circe.generic.auto._
    import io.circe.syntax._

    lazy val toJson: String = AccessValidationResponse.printer.print(this.asJson)
  }

  object AccessValidationResponse {
    private[AccessValidationResponse] val printer = Printer.spaces2SortKeys.copy(dropNullValues = false)
    val Forbidden: AccessValidationResponse = new AccessValidationResponse("ACCESS_FORBIDDEN")
    val Malformed: AccessValidationResponse = new AccessValidationResponse("MALFORMED")
    val UnauthorizedSNF: AccessValidationResponse = new AccessValidationResponse("SESSION_NOT_FOUND")
    val UnauthorizedLocked: AccessValidationResponse = new AccessValidationResponse("LOCKED")
    val UnauthorizedExpired: AccessValidationResponse = new AccessValidationResponse("EXPIRED")
    val UnauthorizedInvalidClaim: AccessValidationResponse = new AccessValidationResponse("INVALID_CLAIM")
  }

  val respOk: CheckResponse = CheckResponse(None, CheckResponse.HttpResponse.OkResponse(OkHttpResponse(Nil)))

  val respForbidden: CheckResponse = notOkResp(StatusCode.Forbidden, AccessValidationResponse.Forbidden)

  private def notOkResp(statusCode: StatusCode, accessValidationResponse: AccessValidationResponse) =
    CheckResponse(
      /** Some thing that is not 200/Ok */
      Some(Status(400)),
      CheckResponse.HttpResponse.DeniedResponse(
        DeniedHttpResponse(
          Some(HttpStatus(statusCode)),
          Seq(HeaderValueOption(Some(HeaderValue("Content-Type", "application/json")))),
          accessValidationResponse.toJson),
      )
    )
}
