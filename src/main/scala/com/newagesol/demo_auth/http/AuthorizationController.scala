package com.newagesol.demo_auth.http

import com.newagesol.common.akka_http.{BaseEndpoint, Controller}

import scala.concurrent.Future

trait AuthorizationController extends Controller {
  abstract override def endpoints: List[BaseEndpoint] = List(
    authEndpointGet, testEndpointGet).map(_.asDocumented) ::: super.endpoints

  val authEndpointGet = endpoint
    .get
    .in("auth")
    .in(queryParams)
    .in(headers)
    .serverLogic[Future] { case (req, headers) =>
      println(s"GET ${req.toMap}")
      println(s"headers $headers")
      Future.successful(Right("ok"))
    }.asTapirEndpoint

  val testEndpointGet = endpoint
    .get
    .in("tests")
    .in(queryParams)
    .in(headers)
    .serverLogic[Future] { case (req, headers) =>
      println(s"GET ${req.toMap}")
      println(s"headers $headers")
      Future.successful(Right("ok"))
    }.asTapirEndpoint
}
