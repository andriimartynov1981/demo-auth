package com.newagesol.demo_auth

import com.newagesol.common.akka_bootstrap._
import com.newagesol.common.akka_grpc_server.GrpcServerModuleImpl
import com.newagesol.common.akka_http.{HealthController, HttpModuleImpl, SwaggerController}
import com.newagesol.demo_auth.grpc.AuthorizationApiService
import com.newagesol.demo_auth.http.AuthorizationController
import com.newagesol.demo_auth.service.AuthorizationService

object App extends Bootstrap(
  new SystemModuleImpl
    with ConfigModuleImpl
    with GrpcServerModuleImpl
    with Http
    with AuthorizationApiService
    with AuthorizationService {

  }
)

trait Http extends HttpModuleImpl
  with HealthController with SwaggerController
  with AuthorizationController { self: SystemModule with ConfigModule => }
