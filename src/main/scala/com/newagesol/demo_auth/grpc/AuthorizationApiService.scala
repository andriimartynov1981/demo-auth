package com.newagesol.demo_auth.grpc

import com.newagesol.common.akka_bootstrap.{ConfigModule, SystemModule}
import com.newagesol.common.akka_grpc_server.{GrpcServerModule, GrpcService, GrpcServiceHandler}
import com.newagesol.demo_auth.service.AuthorizationService
import io.envoyproxy.envoy.service.auth.v2.AuthorizationHandler

trait AuthorizationApiService extends GrpcService { self: AuthorizationService
  with GrpcServerModule with SystemModule with ConfigModule =>

  abstract override def grpcServices: List[GrpcServiceHandler] = {
    AuthorizationHandler.partial(authorizationService) :: super.grpcServices
  }

}
